## reset.css

This file resets basic CSS quirks and visually improves UX.

- **box-sizing: border-box** — makes sure that the total width and height of an element includes it's border or padding.
- **scroll-margin-block: 5ex** — sets the scroll margin block for the :target element to 5 ex.
- **scroll-behavior: smooth** — enables smooth scrolling for the HTML element when prefers-reduced-motion is set to no-preference.
- **min-height: 100vh** — sets a minimum height for the body element to fill the available vertical space.
- **margin: 0** — removes default margins from the body element.
- **-webkit-font-smoothing: antialiased** — improves the appearance of text by smoothing out jagged edges.
- **text-wrap: balance** — balances the text within the h1, h2, h3, and h4 elements.

## sr-only.css

Contains **.sr-only** class that hides an element from view, but makes it accessible to screen readers.
