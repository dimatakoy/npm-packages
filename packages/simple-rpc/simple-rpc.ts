import type { Infer, Type } from '@badrap/valita';

export type ActionDefinition<TSchema extends Type, THandlerResult extends unknown> = {
  schema: TSchema;
  handler: (payload: Infer<TSchema>) => Promise<THandlerResult>;
};

export function defineAction<TSchema extends Type, THandlerResult>(
  action: ActionDefinition<TSchema, THandlerResult>,
) {
  return action;
}

export type SchemaAction = {
  schema: Type;
  handler: (...args: any) => unknown;
};

type Client<TSchema extends Record<string, SchemaAction>> = {
  [m in keyof TSchema]: (payload: Infer<TSchema[m]['schema']>) => ReturnType<TSchema[m]['handler']>;
};

export function defineClient<TSchema extends Record<string, SchemaAction>>(opts: {
  request: (method: string, payload: unknown) => Promise<unknown>;
}) {
  return new Proxy({} as Client<TSchema>, {
    get(_target, property: string) {
      return (payload: object) => opts.request(property, payload);
    },
  });
}
