import * as v from '@badrap/valita';
import { expect, expectTypeOf, test, vi } from 'vitest';
import { defineAction, defineClient } from './simple-rpc.js';

const schema = {
  echo: defineAction({
    schema: v.object({
      name: v.string(),
    }),
    async handler(payload) {
      return payload;
    },
  }),
};

test('defineAction returns schema', () => {
  const action = schema.echo;

  expectTypeOf(action.schema).toMatchTypeOf<
    v.ObjectType<
      {
        name: v.Type<string>;
      },
      undefined
    >
  >;

  expect(action.schema.name).toBe('object');
});

test('defineAction returns handler', () => {
  const action = schema.echo;

  expectTypeOf(action.handler).toMatchTypeOf<
    (payload: { name: string }) => Promise<{
      name: string;
    }>
  >;

  expect(action.handler).toBeTypeOf('function');
});

test('defineClient returns client', () => {
  const client = defineClient<typeof schema>({
    request: vi.fn(),
  });

  expectTypeOf(client.echo).toMatchTypeOf<
    (payload: { name: string }) => Promise<{
      name: string;
    }>
  >;

  expect(client.echo).toBeTypeOf('function');
});

test('client calls request', async () => {
  const request = vi.fn().mockReturnValueOnce({ name: 'hello' });
  const client = defineClient<typeof schema>({
    request,
  });

  const result = await client.echo({ name: 'hello' });

  expect(request).lastCalledWith('echo', { name: 'hello' });
  expect(result).toStrictEqual({ name: 'hello' });
});
