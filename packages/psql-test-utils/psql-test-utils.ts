import type { Client } from 'pg';
import pg from 'pg';

export async function createTestDatabase(client: Client, template: string) {
  const testDatabaseId = crypto.randomUUID();

  const name = `test_${testDatabaseId}`;

  const escapedName = pg.escapeIdentifier(name);
  const escapedTemplate = pg.escapeIdentifier(template);

  const query = `create database ${escapedName} template ${escapedTemplate}`;

  await client.query(query);

  return {
    get name() {
      return name;
    },

    async destroy() {
      await client.query(`drop database ${escapedName}`);
    },
  };
}
