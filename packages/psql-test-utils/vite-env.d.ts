/// <reference types="vite/client" />

interface ImportMetaEnv {
  readonly VITE_POSTGRESQL_URL: string | undefined;
}

interface ImportMeta {
  readonly env: ImportMetaEnv;
}
