# @dimatakoy/psql-test-utils

This package exports only one function `createTestDatabase` which helps run integration tests with PostgreSQL.

## Usage example

```JS
import pg from 'pg';
import { createTestDatabase } from '@dimatakoy/psql-test-utils';

const client = new pg.Client({ connectionString: String(import.meta.env.VITE_POSTGRESQL_URL) });
await client.connect();

// Creates a database based from template
const db = await createTestDatabase(client, 'template0');

// "test_9bde29d4-51f9-4af0-a042-b00752739a2d"
db.name

// Deletes the test database
await db.destroy();

```
