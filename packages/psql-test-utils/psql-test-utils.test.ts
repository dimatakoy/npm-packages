import assert from 'node:assert/strict';
import { afterAll, test } from 'vitest';

import type { Client } from 'pg';
import pg from 'pg';

import { createTestDatabase } from './psql-test-utils.js';

const client = new pg.Client({ connectionString: String(import.meta.env.VITE_POSTGRESQL_URL) });

await client.connect();

afterAll(async () => {
  await client.end();
});

async function isDatabaseExists(client: Client, name: string) {
  const query = await client.query(`SELECT 1 FROM pg_database WHERE datname = $1`, [name]);

  return query.rows.length === 1;
}

test('creates database', async () => {
  const db = await createTestDatabase(client, 'template0');

  const isExists = await isDatabaseExists(client, db.name);

  assert.ok(isExists);
});

test('destroys database', async () => {
  const db = await createTestDatabase(client, 'template0');

  await db.destroy();

  const isExists = await isDatabaseExists(client, db.name);

  assert.ok(!isExists);
});
