const SECOND = 1000;
const MINUTE = SECOND * 60;
const HOUR = MINUTE * 60;
const DAY = HOUR * 24;

type Session = {
  expiresAt: number;
};

type SessionContext = {
  isWithinExpiration: (expiresAt: number) => boolean;
  isActivePeriodExpired: (expiresAt: number) => boolean;
};

type SessionOutcome = 'session' | 'session_expired' | 'session_near_expiration';

export function isWithinExpiration(expiresAt: number) {
  return expiresAt > Date.now();
}

export function isActivePeriodExpired(expiresAt: number) {
  return !isWithinExpiration(expiresAt - DAY * 15);
}

export function getSessionStateOutcome(context: SessionContext, session: Session): SessionOutcome {
  if (!context.isWithinExpiration(session.expiresAt)) {
    return 'session_expired';
  }

  if (context.isActivePeriodExpired(session.expiresAt)) {
    return 'session_near_expiration';
  }

  return 'session';
}
