import { expect, test } from 'vitest';
import {
  getSessionStateOutcome,
  isActivePeriodExpired,
  isWithinExpiration,
} from './simple-session.js';

const SECOND = 1000;
const MINUTE = SECOND * 60;
const HOUR = MINUTE * 60;
const DAY = HOUR * 24;

test('isWithinExpiration', async () => {
  expect(isWithinExpiration(Date.now() + 1)).toBe(true);
  expect(isWithinExpiration(Date.now())).toBe(false);
});

test('isActivePeriodExpired', async () => {
  const live = Date.now() + DAY * 31;
  const dead = Date.now() + DAY * 14;

  expect(isActivePeriodExpired(live)).toBe(false);
  expect(isActivePeriodExpired(dead)).toBe(true);
});

test('session expired', async () => {
  const ctx = {
    isWithinExpiration,
    isActivePeriodExpired,
  };

  const session = {
    id: 'id',
    expiresAt: Date.now(),
  };

  const result = getSessionStateOutcome(ctx, session);

  expect(result).toBe('session_expired');
});

test('session needs revalidate', async () => {
  const ctx = {
    isWithinExpiration,
    isActivePeriodExpired,
  };

  const session = {
    id: 'id',
    expiresAt: Date.now() + DAY * 1,
  };

  const result = getSessionStateOutcome(ctx, session);

  expect(result).toBe('session_near_expiration');
});

test('session is fresh', async () => {
  const ctx = {
    isWithinExpiration,
    isActivePeriodExpired,
  };

  const session = {
    id: 'id',
    expiresAt: Date.now() + DAY * 31,
  };

  const result = getSessionStateOutcome(ctx, session);

  expect(result).toBe('session');
});
